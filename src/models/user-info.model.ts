export interface IUserInfo {
    balance: number;
    email: string;
    id: number;
    name: string;
};
  