import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, of, from } from 'rxjs';
import { tap, switchMap } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { Storage } from '@ionic/storage';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
  constructor(
    private _storage: Storage,
    private authService: AuthService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    console.log(55555, this._storage.get('access_token'), req, next)
    // const token = this.browserStorage.getFromLocalStorage(EStorageKeys.TOKEN);
    return from( this._storage.get('access_token')).pipe(
      switchMap(tokenData => {
        const data = tokenData;
        let token = JSON.parse(data);
        const authHeader = token ? {Authorization: `Bearer ${token}`} : {};
    
        const request = req.clone({
          setHeaders: {
            ...authHeader
          }
        });
        return next.handle(request).pipe(
          tap(
            event => {},
            err => {
              if (err instanceof HttpErrorResponse && err.status === 401) {
                console.warn('Unauthorized');
                // this.authService.logout$();
              }
            }
          )
        );

      }),
      );
  }
}
