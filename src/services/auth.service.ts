import { Injectable } from '@angular/core';
import { of, BehaviorSubject, throwError, from } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HTTP, HTTPResponse } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';
import { map, switchMap } from 'rxjs/operators';
import { Platform } from '@ionic/angular';
import { DBService } from './db.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _baseUrl = environment.baseUrl;

  public errorMessage$ = new BehaviorSubject<string>(null);

  public hasAuth$ = new BehaviorSubject<boolean>(null);

  constructor(
    private router: Router,
    protected http: HTTP,
    private storage: Storage,
    private dbService: DBService,
    private platform: Platform
  ) { }

  public async login(email: string, password: string) {
    this.http.setDataSerializer("json");
    const body = {
      email: email,
      password: password
    };
    return await this.http.post(`${this._baseUrl}sessions/create`, body, {
      'Content-Type': 'application/json'
    }).then(
      (response: HTTPResponse) => {
        console.log('login http response', response)
        this.processLoginResponse(JSON.parse(response.data).id_token)
        return JSON.parse(response.data);
      })
      // .catch(error => {
      //   console.log(error);
      //   this.errorMessage$.next(error.message);
      //   return throwError(error)
      // });
    // return from(response).pipe(map(data => data.id_token));
  }

  public async regist(email: string, password: string, username: string) {
    const body = {
      email: email,
      password: password,
      username: username
    };

    await this.http.post(`${this._baseUrl}users`, body, {
      'Content-Type': 'application/json'
    }).then(
      (response: HTTPResponse) => {
        const token = response.data.id_token;
        this.processLoginResponse(token);
        return token;
      })
      // .catch(error => {
      //   this.errorMessage$.next(error.message);
      //   return throwError(error)
      // })
  }

  private async processLoginResponse(token: string) {
    if (this.platform.platforms().includes('desktop')) {
      await this.storage.set('access_token', JSON.stringify(token));
    } else {
      console.log('processLoginResponse', token);
      await this.dbService.setToken(JSON.stringify(token));
    }

    this.setAuth(token);
    this.router.navigateByUrl('/');
  }

  private setAuth(token: string | null) {
    if (token === null) {
      this.hasAuth$.next(false);
    } else {
      this.hasAuth$.next(true);
    }
  }

  public checkAuthForGuard$() {
    return from(this.dbService.wrapGetToken()).pipe(
      map(data => {
        this.setAuth(data);
      }),
      switchMap(() => of(this.hasAuth$.value))
    );
  }

  public async logout() {
    if (this.platform.platforms().includes('desktop')) {
      if (this.storage.get('access_token') === null) {
        await this.storage.clear();
      } else {
        await this.storage.remove('access_token');
      }
    } else {
      await this.dbService.removeToken();
    }
    this.setAuth(null);
    this.router.navigateByUrl(`/sign-in`);
    return of(true);
  }

}