import { Injectable } from '@angular/core';
import { BehaviorSubject, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IUserInfo } from '../models/user-info.model';
import { HTTP } from '@ionic-native/http/ngx';
import { ITransaction } from 'src/models/transaction.model';
import { DBService } from './db.service';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  private _baseUrl = environment.baseUrl;

  public errorMessage$ = new BehaviorSubject<string>(null);

  userInfo$ = new BehaviorSubject<IUserInfo>(null);
  getUserInfoStatus$ = new BehaviorSubject('peding');

  constructor(
    private http: HTTP,
    private dbService: DBService,
    private platform: Platform
  ) { }

  public async getUserInfo() {
    await this.dbService.wrapGetToken().then(async tokenData => {
      const token = JSON.parse(tokenData);
      return await this.http.get(`${this._baseUrl}api/protected/user-info`, {}, {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
      .then(async data => {
        console.log('data - getUserInfo$', JSON.parse(data.data));
        this.getUserInfoStatus$.next('success');
        const userInfo = JSON.parse(data.data).user_info_token;
        this.userInfo$.next(userInfo);
        if(!this.platform.platforms().includes('desktop')) {
          this.dbService.setUserInfo(userInfo);
        }
      })
      .catch(error => {
        console.log('error - getUserInfo$', error);
        this.getUserInfoStatus$.next('error');
        if(!this.platform.platforms().includes('desktop')) {
          this.dbService.getUserInfo().then(userInfo => {
            this.userInfo$.next(userInfo);
          });
        }
        this.errorMessage$.next(error.message);
        return throwError(error)
      });
    });
    
  }

  public async getUsersList(filter: string = '') {
    let response;
    await this.dbService.wrapGetToken().then(async tokenData => {
      let body = {
        filter: filter
      }
      const token = JSON.parse(tokenData);
      this.http.setDataSerializer("json");
      return await this.http.post(`${this._baseUrl}api/protected/users/list`, body, { 
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
      .then(async data => {
        console.log('data - getTransactionsList', JSON.parse(data.data));
        response = JSON.parse(data.data);
      })
      .catch(error => {
        console.log('error - getTransactionsList', error);
        this.errorMessage$.next(error.message);
        return throwError(error)
      });
    });

    return response; 
  }

  public async getTransactionsList() {
    let response;
    await this.dbService.wrapGetToken().then(async tokenData => {
      const token = JSON.parse(tokenData);
      return await this.http.get(`${this._baseUrl}api/protected/transactions`, {}, { 
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
      .then(async data => {
        console.log('data - getTransactionsList', JSON.parse(data.data));
        response = JSON.parse(data.data);
      })
      .catch(error => {
        console.log('error - getTransactionsList', error);
        this.errorMessage$.next(error.message);
        return throwError(error)
      });
    });
    return response;
  }

  public async createTransaction(name: string, amount: number): Promise<ITransaction> {
    let response;
    await this.dbService.wrapGetToken().then(async tokenData => {

      const token = JSON.parse(tokenData);
      let body = {
        name: name,
        amount: amount
      }
      this.http.setDataSerializer("json");
      return await this.http.post(`${this._baseUrl}api/protected/transactions`, body, { 
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }).then(data => {
        response = JSON.parse(data.data);
      }).catch(error => {
        console.log('error - getTransactionsList', error);
        this.errorMessage$.next(error.message);
        return throwError(error)
      });
    });
    return response;
  }

}