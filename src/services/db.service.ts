import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { ITransaction } from 'src/models/transaction.model';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { IUserInfo } from 'src/models/user-info.model';

@Injectable({
  providedIn: 'root'
})
export class DBService {

  databaseObj: SQLiteObject;

  constructor(
    private sqlite: SQLite,
    private _storage: Storage,
    private platform: Platform
  ) { }

  public init() {
    this.sqlite.create({
      name: "pw_database",
      location: 'default'
    }).then((res) => {
      this.databaseObj = res;

      this.databaseObj.executeSql("CREATE TABLE IF NOT EXISTS transactionslist (id integer, date VARCHAR(255), username VARCHAR(255), amount integer, balance integer)", []).then(() => {
        console.log("Table Created - transactionslist");
      }).catch((err) => {
        console.log(err);
        console.log("Table Creation Error - transactionslist: " + err);
      });

      this.databaseObj.executeSql("CREATE TABLE IF NOT EXISTS accesstoken (token VARCHAR(255))", []).then(() => {
        console.log("Table Created - accesstoken");
      }).catch((err) => {
        console.log(err);
        console.log("Table Creation Error - accesstoken: " + err);
      });

      this.databaseObj.executeSql("CREATE TABLE IF NOT EXISTS userinfo (balance integer, email VARCHAR(255), id integer, name VARCHAR(255))", []).then(() => {
        console.log("Table Created - userinfo");
      }).catch((err) => {
        console.log(err);
        console.log("Table Creation Error - userinfo: " + err);
      });

    }).catch(err => {
      console.log(err);
    });
  }

  public insert(transaction: ITransaction) {
    this.databaseObj.executeSql(
      "INSERT INTO transactionslist (id, date, username, amount, balance) VALUES (?,?,?,?,?)",
      [transaction.id, transaction.date, transaction.username, transaction.amount, transaction.balance]
    ).then(() => {
      console.log("Inserted");
    }).catch((err) => {
      console.log("Insert Error" + err);
    })
  }

  public async readAllData() {
    let response;
    await this.databaseObj.executeSql("SELECT * FROM transactionslist", []).then(res => {
      console.log(res);

      let tmp = [];
      for (let i = 0; i < res.rows.length; i++) {
        tmp.push({
          id: res.rows.item(i).id,
          date: res.rows.item(i).date,
          username: res.rows.item(i).username,
          amount: res.rows.item(i).amount,
          balance: res.rows.item(i).balance
        });
      }
      console.log('readAllData - tmp', tmp);
      response = tmp;

    }).catch(() => {
      console.log("READ ERROR");
    })
    return response;
  }
  
  public async getUserInfo() {
    let userInfo: IUserInfo;
    await this.databaseObj.executeSql("SELECT * FROM userinfo", []).then(res => {
      console.log('getUserInfo', res.rows.item(0));
      // token = (res.rows.item(0) && res.rows.item(0).token) ? res.rows.item(0).token : null;
      userInfo = {
        balance: res.rows.item(0).balance,
        email: res.rows.item(0).email,
        id: res.rows.item(0).id,
        name: res.rows.item(0).name,
      };
    })
    return userInfo;
  }

  public async setUserInfo(userInfo: IUserInfo) {
    await this.databaseObj.executeSql("SELECT * FROM userinfo", []).then(async res => {
      if (res.rows.item(0)) {
        await this.databaseObj.executeSql(
          "UPDATE userinfo SET balance=?, email=?, id=?, name=?", 
          [userInfo.balance, userInfo.email, userInfo.id, userInfo.name]
        ).then((answer) => {
          console.log("userinfo is updated", answer.rows.item(0))
        }).catch((err) => {
          console.log("Update Error" + err);
        })
      } else {
        this.databaseObj.executeSql(
          "INSERT INTO userinfo (balance, email, id, name) VALUES (?,?,?,?)",
          [userInfo.balance, userInfo.email, userInfo.id, userInfo.name]
        ).then(() => {
          console.log("userinfo is inserted", res.rows.item(0));
        }).catch((err) => {
          console.log("Insert Error" + err);
        })
      }
    })
  }

  public async getToken() {
    let token;
    await this.databaseObj.executeSql("SELECT * FROM accesstoken", []).then(res => {
      console.log('getToken', res.rows.item(0));
      token = (res.rows.item(0) && res.rows.item(0).token) ? res.rows.item(0).token : null;
    })
    return token;
  }

  public async setToken(newToken) {
    await this.databaseObj.executeSql("SELECT * FROM accesstoken", []).then(async res => {
      if (res.rows.item(0)) {
        await this.databaseObj.executeSql("UPDATE accesstoken SET token=?", [newToken]).then((answer) => {
          console.log("token is updated", answer.rows.item(0))
        }).catch((err) => {
          console.log("Update Error" + err);
        })
      } else {
        console.log('setToken newToken', newToken);
        await this.databaseObj.executeSql("INSERT INTO accesstoken (token) VALUES (?)", [newToken]).then(async response => {
          console.log("token is inserted", newToken, 'response', response.rows.item(0))
        }).catch((err) => {
          console.log("Insert Error" + err);
        });
      }
    });
  }

  public async removeToken() {
    return await this.databaseObj.executeSql("DELETE FROM accesstoken", []).then(() => {
      console.log("Deleted")
    }).catch((err) => {
      console.log("Delete Error" + err);
    })
  }

  public async wrapGetToken() {
    console.log('includes desktop', this.platform.platforms().includes('desktop'))
    if (this.platform.platforms().includes('desktop')) {
      return await this._storage.get('access_token')
    } else {
      return await this.getToken();
    }
  }
}