import { Component, OnInit } from '@angular/core';
import { ITransaction } from 'src/models/transaction.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { MainService } from 'src/services/main.service';
import { DBService } from 'src/services/db.service';

@Component({
  selector: 'app-transactions-list',
  templateUrl: './transactions-list.page.html',
  styleUrls: ['./transactions-list.page.scss'],
})
export class TransactionsListPage implements OnInit {

  transactionsList: ITransaction[] = [];

  getTransactionsListStatus$ = new BehaviorSubject<string>('pending');

  constructor(private mainService: MainService, private dbService: DBService) { }

  ngOnInit(): void {
    this.mainService.getTransactionsList().then(
      (response: any) => {
        console.log(response);
        this.getTransactionsListStatus$.next('success');
        this.transactionsList = response.trans_token;
      }).catch(() => {
        this.getTransactionsListStatus$.next('error');
        this.dbService.readAllData().then((response: any) => {
          this.transactionsList = response;
          console.log('1Transactions-db readAllData', this.transactionsList);
        });
      });
  }

  sortTransactions(type: string) {
    switch (type) {
      case 'dateUp': {
        this.transactionsList.sort((a, b) => {
          const dateA: any = new Date(a.date), dateB: any = new Date(b.date);
          return this.doSorting('up', dateA, dateB);
        })
      }
        break;

      case 'dateDown': {
        this.transactionsList.sort((a, b) => {
          const dateA: any = new Date(a.date), dateB: any = new Date(b.date);
          return this.doSorting('down', dateA, dateB);
        })
      }
        break;

      case 'nameUp': {
        this.transactionsList.sort((a, b) => {
          const nameA: any = a.username.toLowerCase(), nameB: any = b.username.toLowerCase();
          return this.doSorting('up', nameA, nameB);
        })
      }
        break;

      case 'nameDown': {
        this.transactionsList.sort((a, b) => {
          const nameA: any = a.username.toLowerCase(), nameB: any = b.username.toLowerCase();
          return this.doSorting('down', nameA, nameB);
        })
      }
        break;

      case 'amountUp': {
        this.transactionsList.sort((a, b) => {
          const amountA: any = Math.abs(a.amount), amountB: any = Math.abs(b.amount);
          return this.doSorting('up', amountA, amountB);
        })
      }
        break;

      case 'amountDown': {
        this.transactionsList.sort((a, b) => {
          const amountA: any = Math.abs(a.amount), amountB: any = Math.abs(b.amount);
          return this.doSorting('down', amountA, amountB);
        })
      }
        break;

      default: return;
    }
  }

  doSorting(direction, optA, optB) {
    console.log(direction, optA, optB);
    if (direction === 'up') {
      return optA - optB;
    } else if (direction === 'down') {
      return optB - optA;
    } else {
      return;
    }
  }

}
