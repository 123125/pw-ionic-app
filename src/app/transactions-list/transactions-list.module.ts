import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransactionsListPageRoutingModule } from './transactions-list-routing.module';

import { TransactionsListPage } from './transactions-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransactionsListPageRoutingModule
  ],
  declarations: [TransactionsListPage]
})
export class TransactionsListPageModule {}
