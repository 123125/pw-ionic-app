import { Component, OnInit } from '@angular/core';
import { UserInfoPage } from '../user-info/user-info.page';
import { TransactionsListPage } from '../transactions-list/transactions-list.page';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
