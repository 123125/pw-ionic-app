import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'user-info',
        loadChildren: () => import('../user-info/user-info.module').then( m => m.UserInfoPageModule)
      },
      {
        path: 'transactions-list',
        loadChildren: () => import('../transactions-list/transactions-list.module').then( m => m.TransactionsListPageModule)
      },
      
      {
        path: '',
        redirectTo: '/user-info',
        pathMatch: 'full'
      },
  ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
