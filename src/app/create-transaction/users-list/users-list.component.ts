import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { map, takeUntil, switchMap, debounceTime, distinctUntilChanged, takeWhile, tap, catchError } from 'rxjs/operators';
import { Subject, BehaviorSubject, timer, throwError, of, Observable } from 'rxjs';

import { MainService } from '../../../services/main.service';
import { ModalController } from '@ionic/angular';


export interface User {
  id: number,
  name: string
}

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  @Output() getSelectUser = new EventEmitter<User>();

  @Input() selectedUser: User;

  usersList: any;

  getUsersListStatus$ = new BehaviorSubject<string>('');

  onDestroy$ = new Subject();

  constructor(private mainService: MainService, public modalCtrl: ModalController) { }

  ngOnInit(): void {}

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  getUsersList(filter: string) {
    this.onDestroy$.next();
    timer(300).pipe(
      takeUntil(this.onDestroy$),
      tap(() => {
        if (filter === '') this.usersList = [];
      }),
      takeWhile(() => filter !== ''),
      tap(() => this.getUsersListStatus$.next('pending')),
      switchMap(() => {
        
        this.mainService.getUsersList(filter).then(response => {
          this.usersList = response;
        });
        this.getUsersListStatus$.next('success')
        return of();
      }),
      catchError(error => {
        this.getUsersListStatus$.next('error');
        this.usersList = [];
        return throwError(error);
      }),
    ).subscribe();
  }

  selectUser(user: User) {
    this.selectedUser = user;
    // this.getSelectUser.emit(user);
    this.modalCtrl.dismiss({
      'selectedUser': user
    });
  }
  
}
