import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { MainService } from 'src/services/main.service';
import { takeUntil, map } from 'rxjs/operators';
import { ModalController, ToastController, Platform } from '@ionic/angular';
import { UsersListComponent } from './users-list/users-list.component';
import { Router, ActivatedRoute } from '@angular/router';
import { DBService } from 'src/services/db.service';

@Component({
  selector: 'app-create-transaction',
  templateUrl: './create-transaction.page.html',
  styleUrls: ['./create-transaction.page.scss'],
})
export class CreateTransactionPage implements OnInit {

  public formGroup: FormGroup;

  selectedUser = '';

  onDestroy$ = new Subject();
  onSearch$ = new Subject();

  constructor(
    private mainService: MainService, 
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private modalController: ModalController,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private dbService: DBService,
    private platform: Platform
  ) {
    this.formGroup = this.formBuilder.group({name: null, amount: null});
    this.formGroup.controls.name.setValidators([Validators.required]);
    this.formGroup.controls.amount.setValidators([Validators.required]);

    this._activatedRoute.queryParams.pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(queryParams => {
      if (queryParams.username) this.formGroup.controls.name.setValue(queryParams.username);
      if (queryParams.amount) this.formGroup.controls.amount.setValue(Math.abs(queryParams.amount));
    })
  }

  ngOnInit(): void {}

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
    this.onSearch$.complete();
  }

  submit() {
    this.mainService.userInfo$.pipe(
      map(userInfo => userInfo.balance),
      takeUntil(this.onSearch$),
      takeUntil(this.onDestroy$),
    ).subscribe(balance => {
      this.onSearch$.next();
      if (balance > this.formGroup.value.amount) {
        this.mainService.createTransaction(this.formGroup.value.name, this.formGroup.value.amount).then(
          (response: any) => {
            if(!this.platform.platforms().includes('desktop')) {
              this.dbService.insert(response.trans_token);
            }
          }
        )
        .then( () => {
          this._router.navigate(['/transactions-list'])
        })
      } else {
        this.presentToastWithOptions();
      }
    });
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: UsersListComponent,
      cssClass: 'my-custom-class'
    });
     modal.present();
    return await modal.onWillDismiss().then(data => {
      this.formGroup.controls.name.setValue(data.data['selectedUser'].name);
    });
  }

  async presentToastWithOptions() {
    const toast = await this.toastCtrl.create({
      message: 'Sorry, you don\'t have emough money!',
      position: 'middle',
      cssClass: 'ion-text-center',
      duration: 2000
    });
    toast.present();
  }

}
