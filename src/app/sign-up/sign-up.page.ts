import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { SignUpFormModel } from '../../models/sign-up.model';
import { ToastController } from '@ionic/angular';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {

  public formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private toastCtrl: ToastController
  ) { 
    this.formGroup = this.formBuilder.group(new SignUpFormModel());
    this.formGroup.controls.username.setValidators([Validators.required]);
    this.formGroup.controls.email.setValidators([Validators.required, Validators.email]);
    this.formGroup.controls.password.setValidators([Validators.required]);
  }

  ngOnInit(): void {
  }

  submit() {
    this.authService.regist(this.formGroup.value.email, this.formGroup.value.password, this.formGroup.value.username).catch(error => {
      this.presentToast(error.error);
      return throwError(error)
    });;
  }

  async presentToast(text: string) {
    const toast = await this.toastCtrl.create({
      message: text,
      position: 'middle',
      cssClass: 'ion-text-center',
      duration: 2000
    });
    toast.present();
  }

}
