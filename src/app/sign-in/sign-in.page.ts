import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/services/auth.service';
import { SignInFormModel } from 'src/models/sign-in.model';
import { ToastController } from '@ionic/angular';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.page.html',
  styleUrls: ['./sign-in.page.scss'],
})
export class SignInPage implements OnInit {

  public formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private toastCtrl: ToastController
  ) { 
    this.formGroup = this.formBuilder.group(new SignInFormModel());
    this.formGroup.controls.email.setValidators([Validators.required, Validators.email]);
    this.formGroup.controls.password.setValidators([Validators.required]);
  }

  ngOnInit(): void {
  }

  submit(e: Event) {
    this.authService.login(this.formGroup.value.email, this.formGroup.value.password).catch(error => {
      this.presentToast(error.error);
      return throwError(error)
    });
  }

  async presentToast(text: string) {
    const toast = await this.toastCtrl.create({
      message: text,
      position: 'middle',
      cssClass: 'ion-text-center',
      duration: 2000
    });
    toast.present();
  }

}
