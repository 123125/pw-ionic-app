import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
// import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { TabsPage } from './tabs/tabs.page';
import { AuthService } from 'src/services/auth.service';
import { MainService } from 'src/services/main.service';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DBService } from 'src/services/db.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  rootPage: any = TabsPage;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public authService: AuthService,
    private mainService: MainService,
    private dbService: DBService
  ) {
    this.initializeApp();    
  }

  ngOnInit() {}

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      if (!this.platform.platforms().includes('desktop')) {
        this.dbService.init();
      }

    });
  }

  ngOnDestroy() {
    this.authService.logout();
  }
}
