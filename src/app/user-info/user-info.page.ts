import { Component, OnInit } from '@angular/core';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.page.html',
  styleUrls: ['./user-info.page.scss'],
})
export class UserInfoPage implements OnInit {

  constructor(public mainService: MainService) { }

  ngOnInit(): void {
    
  }

  ionViewDidEnter () {
    this.mainService.getUserInfo();
  }

}
